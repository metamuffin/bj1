import { AudioEmitter } from "./audio.ts";
import { Entity } from "./entities/entity.ts";
import { Position, Vec2 } from "./model.ts";
import { World } from "./world.ts";

export interface PlayerSpeed {
    forward: number
    side: number
    rotation: number
}

export class Player extends Entity {
    position: Position = { x: 0, y: 0 }
    rotation = 0

    walking_sound: AudioEmitter
    keys_pressed = new Set<string>()

    speed: PlayerSpeed = { rotation: 0, forward: 0, side: 0 }

    constructor(protected world: World) {
        super(world)
        this.walking_sound = new AudioEmitter(this.world.audio_controller, {
            // path: "/a/walking.ogg",
            path: "/a/test/Simple_Template_000.export.phrase1.opus",
            fade: 0.5,
            loop: true,
        })

        document.addEventListener("keydown", ev => {
            if (ev.repeat) return
            this.keys_pressed.add(ev.code)
            this.key_event(true, ev.code)
        })

        document.addEventListener("keyup", ev => {
            if (ev.repeat) return
            this.keys_pressed.delete(ev.code)
            this.key_event(false, ev.code)
        })
        document.addEventListener("mousemove", ev => {
            this.rotation += ev.movementX * -0.01
            ev.preventDefault()
        })

        this.update_autocleanup({ debug_label: "player input", run: () => this.update() })
    }

    get direction(): Vec2 { return { x: Math.sin(this.rotation), y: Math.cos(this.rotation) } }

    key_event(pressed: boolean, key: string) {
        const mult = pressed ? 1 : -1
        if (key == "KeyW") this.speed.forward += 0.04 * mult
        if (key == "KeyS") this.speed.forward -= 0.04 * mult
        if (key == "KeyA") this.speed.side -= 0.03 * mult
        if (key == "KeyD") this.speed.side += 0.03 * mult
        if (key == "KeyQ") this.speed.rotation += 0.05 * mult
        if (key == "KeyE") this.speed.rotation -= 0.05 * mult

        if (this.speed.side != 0 || this.speed.forward != 0) this.walking_sound.play()
        else this.walking_sound.pause()
    }

    update() {
        this.position.x += this.speed.forward * this.direction.x
        this.position.y += this.speed.forward * this.direction.y
        this.position.x += -this.speed.side * this.direction.y
        this.position.y += -this.speed.side * -this.direction.x
        this.rotation += this.speed.rotation

        // const s = this.keys_pressed.has("Space") ? 0.09 : 0.03
        // if (this.keys_pressed.has("KeyF")) this.world.player.position.x -= s
        // if (this.keys_pressed.has("KeyH")) this.world.player.position.x += s
        // if (this.keys_pressed.has("KeyT")) this.world.player.position.y -= s
        // if (this.keys_pressed.has("KeyG")) this.world.player.position.y += s

        // const rs = 0.03
        // if (this.keys_pressed.has("KeyQ")) this.world.player.rotation += rs
        // if (this.keys_pressed.has("KeyE")) this.world.player.rotation -= rs
        // if (this.keys_pressed.has("KeyW")) this.world.player.move(s, 0)
        // if (this.keys_pressed.has("KeyS")) this.world.player.move(-s, 0)
        // if (this.keys_pressed.has("KeyA")) this.world.player.move(0, -s / 2)
        // if (this.keys_pressed.has("KeyD")) this.world.player.move(0, s / 2)

    }
}

