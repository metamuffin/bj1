import { AudioEmitter } from "../audio.ts";
import { Vec2 } from "../model.ts";
import { World } from "../world.ts";
import { Entity } from "./entity.ts";

export interface SoundTriggerOpts {
    position: Vec2,
    size: Vec2
    enter?: string
    leave?: string
    inside?: string
}

export class SoundTrigger extends Entity {
    inside_emitter?: AudioEmitter

    constructor(w: World, protected opts: SoundTriggerOpts) {
        super(w)

        const play = (e: string) => {
            new AudioEmitter(this.world.audio_controller, {
                position: w.player.position,
                autoplay: true,
                destroy_at_end: true,
                path: e
            })
        }
        this.trigger_autocleanup({
            ...opts,
            on_enter: () => {
                if (opts.enter) play(opts.enter)
                if (opts.inside) this.inside_emitter = new AudioEmitter(this.world.audio_controller, {
                    position: w.player.position,
                    autoplay: true,
                    loop: true,
                    path: opts.inside,
                })
            },
            on_leave: () => {
                if (opts.leave) play(opts.leave)
                if (this.inside_emitter) this.inside_emitter.remove()
            }
        })

    }
}
