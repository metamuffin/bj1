import { Trigger, UpdateHook, World } from "../world.ts";

export abstract class Entity {
    cleanup: Set<() => void> = new Set()

    constructor(protected world: World) {
        this.world.entities.add(this)
        this.cleanup.add(() => this.world.entities.delete(this))
    }

    trigger_autocleanup(t: Trigger) {
        this.world.triggers.add(t)
        this.cleanup.add(() => this.world.triggers.remove(t))
    }
    update_autocleanup(u: UpdateHook) {
        this.world.update_hooks.add(u)
        this.cleanup.add(() => this.world.update_hooks.delete(u))
    }

    despawn() { this.cleanup.forEach(f => f()) }

    update() { }
}
