import { AudioEmitter } from "../audio.ts";
import { Position } from "../model.ts";
import { World } from "../world.ts";
import { Entity } from "./entity.ts";

export class Ambient extends Entity {
    emitter: AudioEmitter

    constructor(w: World, protected position: Position, path: string) {
        super(w)
        this.emitter = new AudioEmitter(w.audio_controller, {
            path,
            position: this.position,
            autoplay: true,
            loop: true
        })
    }
}
