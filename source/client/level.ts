import { Ambient } from "./entities/ambient.ts";
import { SoundTrigger } from "./entities/soundtrigger.ts";
import { World } from "./world.ts";


export function create_world() {
    const w = new World()

    new Ambient(w, { x: 3, y: 10 }, "/a/water-flowing.ogg")
    new Ambient(w, { x: -3, y: 0 }, "/a/smith-working.ogg")
    new Ambient(w, { x: 15, y: -10 }, "/a/test/Simple_Template_000.export.phrase5.opus")
    new SoundTrigger(w, {
        position: { x: 10, y: -5 },
        size: { x: 10, y: 15 },
        enter: "/a/door-opening.ogg",
        leave: "/a/door-closing.ogg",
    })
    new SoundTrigger(w, {
        position: { x: 0, y: 10 },
        size: { x: 20, y: 20 },
        inside: "/a/walking-water.ogg",
        leave: "/a/drowning.ogg",
    })

    return w
}