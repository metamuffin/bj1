/// <reference lib="dom" />

import { debug_renderer } from "./debug.ts"
import { create_world } from "./level.ts";

await new Promise<void>(r => {
    const e = document.createElement("h2")
    e.textContent = "click anywhere to begin"
    document.body.append(e)
    const f = () => {
        // document.removeEventListener("keydown", f)
        document.removeEventListener("mousedown", f)
        r()
        e.remove()
    }
    // document.addEventListener("keydown", f)
    document.addEventListener("mousedown", f)
})

const world = create_world()
debug_renderer(world)


document.addEventListener("click", () => document.body.requestPointerLock())

setInterval(() => {
    world.update()
}, 1000 / 100)

