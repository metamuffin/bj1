
export interface Vec2 { x: number, y: number }
export type Position = Vec2

