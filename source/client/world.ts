import { AudioController } from "./audio.ts";
import { CollisionLayer, CollisionShape } from "./collision.ts";
import { DebugData } from "./debug.ts";
import { Entity } from "./entities/entity.ts";
import { Player } from "./player.ts";

export interface Trigger extends CollisionShape, DebugData {
    on_enter?: () => void
    on_leave?: () => void
    triggered?: boolean
}
export interface UpdateHook extends DebugData {
    run: () => void,
    debug_label?: string
}

export class World {
    entities: Set<Entity> = new Set()
    audio_controller = new AudioController(this)
    player: Player

    triggers = new CollisionLayer<Trigger>()
    triggered = new Set<Trigger>()

    update_hooks = new Set<UpdateHook>()

    constructor() { this.player = new Player(this); console.log(this) }

    update() {
        this.audio_controller.update()

        const add_t = (t: Trigger) => {
            if (this.triggered.has(t)) return
            this.triggered.add(t)
            if (t.on_enter) t.on_enter()
        }
        const rem_t = (t: Trigger) => {
            this.triggered.delete(t)
            if (t.on_leave) t.on_leave()
        }

        this.triggered.forEach(t => t.triggered = false)
        this.triggers.intersect(this.player, t => { t.triggered = true; add_t(t) })
        this.triggered.forEach(t => !t.triggered ? rem_t(t) : null)

        this.update_hooks.forEach(h => h.run())
    }
}
