/// <reference lib="dom" />


import { DebugData } from "./debug.ts";
import { Position } from "./model.ts";
import { World } from "./world.ts";

export class AudioController {

    emitters: Set<AudioEmitter> = new Set()
    context: AudioContext

    constructor(
        public world: World,
    ) {
        this.context = new AudioContext()
        // this.context.listener.positionZ.value = 0
        // this.context.listener.forwardZ.value = 0
        // this.context.listener.upX.setValueAtTime(0, this.context.currentTime);
        // this.context.listener.upY.setValueAtTime(1, this.context.currentTime);
        // this.context.listener.upZ.setValueAtTime(0, this.context.currentTime);
        // console.log(this.context.listener);

    }

    update() {
        // this.context.listener.setPosition(this.player.position.x, this.player.position.y, 0)
        // this.context.listener.setOrientation(this.player.direction.x, this.player.direction.y, 0, 0, 1, 0)
        // this.context.listener.positionX.setValueAtTime(this.player.position.x, this.context.currentTime)
        // this.context.listener.positionY.setValueAtTime(this.player.position.y, this.context.currentTime)
        // this.context.listener.forwardX.setValueAtTime(this.player.direction.x, this.context.currentTime)
        // this.context.listener.forwardY.setValueAtTime(this.player.direction.y, this.context.currentTime)
        this.emitters.forEach(e => e.update())
    }
}

export interface AudioEmitterOpts extends DebugData {
    destroy_at_end?: boolean
    autoplay?: boolean
    path: string
    position?: Position
    loop?: boolean
    fade?: number
    gain?: number
}
export class AudioEmitter implements DebugData {
    audio: HTMLAudioElement
    source: MediaElementAudioSourceNode
    panner?: PannerNode
    gain?: GainNode
    fade_pause_timeout?: number;

    get debug_label() { return this.opts.debug_label }
    get debug_color() { return this.opts.debug_color }

    get position() { return this.opts.position }
    set position(p) { this.opts.position = p }
    get path() { return this.opts.path }
    set path(p) { this.opts.path = p }

    constructor(protected c: AudioController, protected opts: AudioEmitterOpts) {
        this.audio = document.createElement("audio")
        this.audio.src = opts.path
        this.audio.loop = opts.loop ?? false
        this.audio.onended = () => {
            if (opts.destroy_at_end) this.remove()
        }
        if (opts.autoplay) this.audio.play()

        this.source = c.context.createMediaElementSource(this.audio)
        let last: AudioNode = this.source

        if (opts.fade || opts.gain) {
            this.gain = c.context.createGain()
            this.gain.gain.setValueAtTime(opts.gain ?? 1, this.c.context.currentTime)
            last.connect(this.gain)
            last = this.gain
        }

        if (this.position) {
            this.panner = c.context.createPanner()
            this.panner.panningModel = 'HRTF';
            this.panner.distanceModel = 'inverse';
            this.panner.refDistance = 1;
            this.panner.maxDistance = 10000;
            this.panner.rolloffFactor = 1;
            this.panner.coneInnerAngle = 360;
            this.panner.coneOuterAngle = 0;
            this.panner.coneOuterGain = 0;
            this.panner.positionX.value = this.position.x
            this.panner.positionY.value = this.position.y
            this.panner.positionZ.value = 0
            this.panner.orientationX.value = 1
            this.panner.orientationY.value = 0
            this.panner.orientationZ.value = 0
            this.source.connect(this.panner)
            last = this.panner
        }


        last.connect(c.context.destination)
        c.emitters.add(this)
    }

    remove() {
        this.c.emitters.delete(this)
        this.panner?.disconnect()
        this.source.disconnect()
    }

    play() {
        this.audio.play()
        if (this.gain) {
            const now = this.c.context.currentTime
            if (this.fade_pause_timeout) clearTimeout(this.fade_pause_timeout)

            if (this.opts.fade)
                this.gain.gain.setTargetAtTime(this.opts.gain ?? 1, now, this.opts.fade)
            else this.gain.gain.setValueAtTime(this.opts.gain ?? 1, now)

        }
    }
    pause() {
        if (this.gain && this.opts.fade) {
            const now = this.c.context.currentTime

            if (this.opts.fade)
                this.gain.gain.setTargetAtTime(0, now, this.opts.fade)
            else this.gain.gain.setValueAtTime(0, now)

            if (this.fade_pause_timeout) clearTimeout(this.fade_pause_timeout)
            this.fade_pause_timeout = setTimeout(() => {
                this.audio.pause()
            }, this.opts.fade * 1200 + 500) // needs to be bigger becuase of inaccurate timeouts in librewolf
        } else {
            this.audio.pause()
        }
    }



    update() {
        if (!this.position || !this.panner) return
        const diff_x = this.position.x - this.c.world.player.position.x
        const diff_y = this.position.y - this.c.world.player.position.y
        const dir = this.c.world.player.direction

        const rx = diff_x * -dir.y + diff_y * dir.x
        const ry = diff_x * -dir.x + diff_y * -dir.y
        this.panner.positionX.value = rx
        this.panner.positionY.value = ry
    }

}



