import { Position, Vec2 } from "./model.ts";

export interface CollisionShape { position: Position, size?: Vec2 }

export class CollisionLayer<T extends CollisionShape> {
    // root: Quad = new Quad()

    items: Set<T> = new Set()

    add(i: T) { this.items.add(i) }
    remove(i: T) { this.items.delete(i) }

    intersect(c: CollisionShape, cb: (i: T) => void) {
        this.items.forEach(i => {
            if (this.collide(i, c)) cb(i)
        })
    }
    all(cb: (i: T) => void) { this.items.forEach(i => cb(i)) }

    collide(a: CollisionShape, b: CollisionShape) {
        if (a.size && b.size)
            return intersect_rect_rect(
                a.position.x, a.position.y, a.size.x, a.size.y,
                b.position.x, b.position.y, b.size.x, b.size.y
            )
        if (a.size)
            return intersect_rect_point(
                a.position.x, a.position.y, a.size.x, a.size.y,
                b.position.x, b.position.y
            )
        if (b.size)
            return intersect_rect_point(
                b.position.x, b.position.y, b.size.x, b.size.y,
                a.position.x, a.position.y
            )
        return false
    }
}

export function intersect_rect_point(
    ax: number, ay: number, aw: number, ah: number,
    bx: number, by: number
) {
    return (
        bx > ax
        && bx < ax + aw
        && by > ay
        && by < ay + ah
    )
}

export function intersect_rect_rect(
    ax: number, ay: number, aw: number, ah: number,
    bx: number, by: number, bw: number, bh: number
) {
    return (
        ax < bx + bw
        && bx < ax + aw
        && ay < by + bh
        && by < ay + ah
    )
}



// interface QuadSet<T extends CollisionShape> { tl: Quad<T>, tr: Quad<T>, bl: Quad<T>, br: Quad<T> }
// class Quad<T extends CollisionShape> {
//     child_quads?: QuadSet<T>
//     child_nodes?: T[]


//     constructor(
//         public size: number,
//         public offx: number,
//         public offy: number
//     ) { }

//     add(n: T) {

//     }
//     remove(n: T) {

//     }

//     should_contains(n: T): boolean {
//         throw new Error("");

//     }

// }



// export interface QNode {
//     x: number
//     y: number
// }
// export const ROOT_QUAD_SIZE = 300000
// export const QUAD_THRESHOLD = 10

// export class Quad {
//     public childs: Quad[] | undefined
//     public nodes: QNode[] = []
//     public size: number
//     public off_x: number
//     public off_y: number
//     constructor(size: number, off_x: number, off_y: number) {
//         this.size = size; this.off_x = off_x; this.off_y = off_y
//     }

//     add(...nodes: QNode[]): void {
//         this.nodes.push(...nodes)
//         if (this.childs) {
//             this.addToChilds(...nodes)
//         } else {
//             if (this.nodes.length > QUAD_THRESHOLD && !this.childs) {
//                 this.split()
//             }
//         }
//     }
//     addToChilds(...nodes: QNode[]) {
//         if (!this.childs) throw new Error("ahhhhhhh nooooooo");
//         for (const n of nodes) {
//             if (!this.shouldInclude(n)) throw new Error("nuuuuuuuuu. this node doesnt belong to us");
//             var fq = this.childs.find(q => q.shouldInclude(n))
//             if (!fq) throw new Error("wtf");
//             fq.add(n)
//         }
//     }
//     split() {
//         this.childs = [
//             new Quad(this.size / 2, this.off_x, this.off_y),
//             new Quad(this.size / 2, this.off_x + this.size / 2, this.off_y),
//             new Quad(this.size / 2, this.off_x, this.off_y + this.size / 2),
//             new Quad(this.size / 2, this.off_x + this.size / 2, this.off_y + this.size / 2)
//         ]
//         this.addToChilds(...this.nodes)
//     }

//     get(x: number, y: number, xs: number, ys: number): QNode[] {
//         if (!this.intersectRect(x, y, xs, ys)) return []
//         if (this.childs) {
//             var n: QNode[] = []
//             for (const c of this.childs) {
//                 n.push(...c.get(x, y, xs, ys))
//             }
//             return n;
//         }
//         if (!this.nodes) throw new Error("sajhhdgsfjahfxbny");
//         return this.nodes
//     }

//     intersectRect(x: number, y: number, xs: number, ys: number) {
//         return (
//             x + xs >= this.off_x
//             && y + ys >= this.off_y
//             && x <= this.off_x + this.size
//             && y <= this.off_y + this.size
//         )
//     }

//     shouldInclude(node: QNode): boolean {
//         return (
//             node.x >= this.off_x
//             && node.y >= this.off_y
//             && node.x < this.off_x + this.size
//             && node.y < this.off_y + this.size
//         )
//     }

//     debugDraw(ctx: CanvasRenderingContext2D, depth?: number) {
//         depth = depth ?? 0
//         ctx.strokeStyle = `hsl(${depth * 50}deg,100%,50%,1)`
//         ctx.lineWidth = 2
//         ctx.beginPath()
//         ctx.rect(this.off_x, this.off_y, this.size, this.size)
//         ctx.stroke()
//         ctx.font = `${Math.min(80, this.size / 10)}px sans-serif`
//         ctx.textBaseline = "middle"
//         ctx.textAlign = "center"
//         ctx.fillText(depth.toString(), this.off_x + this.size / 2, this.off_y + this.size / 2)
//         if (this.childs) for (const q of this.childs) {
//             q.debugDraw(ctx, depth + 1)
//         }
//     }

// }
