import { Position, Vec2 } from "./model.ts";
import { World } from "./world.ts";

export interface DebugData {
    debug_label?: string
    debug_color?: string
}

export interface DebugRenderContext {
    rect_area(pos: Position, size: Position, color?: string): void
    disk_area(pos: Position, radius: number, color?: string): void
    point(pos: Position, color?: string): void
    label(pos: Position, s?: string, color?: string): void
    direction(pos: Position, d: Vec2, color?: string): void
}

export function debug_renderer(world: World) {
    const canvas = document.createElement("canvas")
    canvas.style.position = "absolute"
    canvas.style.top = "0px"
    canvas.style.left = "0px"
    canvas.style.width = "100vw"
    canvas.style.height = "100vh"
    document.body.append(canvas)

    resize()
    globalThis.addEventListener("resize", resize)
    function resize() {
        canvas.width = canvas.clientWidth
        canvas.height = canvas.clientHeight
    }

    const c_ = canvas.getContext("2d")
    if (!c_) throw new Error("asdhjkhjkasfd");
    const c = c_

    const ox = 300, oy = 300, z = 10;

    const dc: DebugRenderContext = {
        rect_area(pos, size, col) {
            c.strokeStyle = col ?? "green"
            c.strokeRect(pos.x * z + ox, pos.y * z + oy, size.x * z, size.y * z)
        },
        disk_area(pos, size, col) {
            c.strokeStyle = col ?? "green"
            c.beginPath()
            c.arc(pos.x * z + ox, pos.y * z + oy, size, 0, Math.PI * 2)
            c.stroke()
        },
        direction(pos, d, col) {
            c.strokeStyle = col ?? "red"
            c.beginPath()
            c.moveTo(pos.x * z + ox, pos.y * z + oy)
            c.lineTo(pos.x * z + ox + d.x * 20, pos.y * z + oy + d.y * 20)
            c.stroke()
        },
        label(pos, s, col) {
            c.fillStyle = col ?? "white"
            c.font = "1em sans-serif"
            c.fillText(s ?? "<none>", pos.x * z + ox, pos.y * z + oy)
        },
        point(p, col) {
            c.strokeStyle = col ?? "white"
            c.beginPath()
            c.arc(p.x * z + ox, p.y * z + oy, 5, 0, Math.PI * 2)
            c.stroke()
        }
    }

    let frame_counter = 0, fps = NaN
    render()
    function render() {
        c.fillStyle = "black"
        c.fillRect(0, 0, canvas.width, canvas.height)

        d_player(world, dc)
        world.audio_controller.emitters.forEach(e => {
            if (!e.position) return
            dc.point(e.position, "magenta")
            dc.label(e.position, e.path)
        })
        world.triggers.all(t => {
            if (t.size) {
                dc.rect_area(t.position, t.size, "red")
            } else dc.point(t.position)
        })

        let d = 20
        let dcol = 20
        const next_col = () => { dcol += 200; d = 20 }
        const write = (s = "") => {
            c.font = "1em sans-serif"
            c.fillText(s, dcol, d)
            d += 20
        }

        write("triggered:")
        world.triggered.forEach(e => write(`\t${e.debug_label ?? "<unnamed>"}`))
        next_col()
        write("update hooks:")
        world.update_hooks.forEach(e => write(`\t${e.debug_label ?? "<unnamed>"}`))
        next_col()
        write("sound emitters:")
        world.audio_controller.emitters.forEach(e => write(`\t${e.debug_label ?? `<unnamed, ${e.path}>`}`))

        next_col()
        write(`fps: ${fps}`)
        frame_counter++
        requestAnimationFrame(render)
    }

    setInterval(() => {
        fps = frame_counter
        frame_counter = 0
    }, 1000)
}

function d_player(world: World, d: DebugRenderContext) {
    d.point(world.player.position)
    d.direction(world.player.position, world.player.direction)
}
